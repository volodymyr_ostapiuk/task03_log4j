package com.ostapiuk;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    public static final String ACCOUNT_SID = "AC9e90cfb10329cfb6e96d90dd3ea3312b";
    public static final String AUTH_TOKEN = "93ad952d869e0a51f30da77a8eee5a0c";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380664248521"),
                        new PhoneNumber("+12312254928"), str).create();
    }
}
